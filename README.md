# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 07: TIM - Encoder Interface

En este ejempo se muestra como usar el Encoder Interface de un Timer de propósito general contando en binario la entrada de un Rotary Encoder (Encoder de cuadratura mecánico).

Entrada del blog: [Programando los TIMs del STM32F1 con libOpenCM3 [Parte 3] – Encoder Interface](https://electronlinux.wordpress.com/2020/07/23/programando-los-tims-del-stm32f1-con-libopencm3-parte-3-encoder-interface/)

Video en YouTube: <>
