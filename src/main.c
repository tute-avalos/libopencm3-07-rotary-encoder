/**
 * @file main.c
 * @author Matías S. Ávalos (Telegra: @tute_avalos)
 * @brief Ejemplo de lectura de rotary encoder con el timer con libOpenCM3
 * @version 0.1
 * @date 2020-07-23
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

int main(void)
{
    // SYSCLK a 72Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    /*
        Se configuran los pinea del PA0 al PA3 como salidas para visualizar
        el cambio en la cuenta.
    */
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_set_mode(
        GPIOA,
        GPIO_MODE_OUTPUT_2_MHZ,
        GPIO_CNF_OUTPUT_PUSHPULL,
        GPIO0 | GPIO1 | GPIO2 | GPIO3);

    // Se habilita el clock del TIM4.
    rcc_periph_clock_enable(RCC_TIM4);

    /* 
        El encoder de cuadratura genera 4 pulsos cuando pasa de una posición
        a la otra y tiene posiciones, por lo tanto 20*4 = 80 pulsos por vuelta.
    */
    timer_set_period(TIM4, 79);

    /*
        Modo encoder 3: Sube y baja el contador por cualquiera de los pulsos en
        TI1FP1 o TI2FP2.
    */
    timer_slave_set_mode(TIM4, TIM_SMCR_SMS_EM3);

    // Configuración de los Canales de Entrada TI1 y TI2:
    timer_ic_set_input(TIM4, TIM_IC1, TIM_IC_IN_TI1);
    timer_ic_set_input(TIM4, TIM_IC2, TIM_IC_IN_TI2);

    // Se habilita el contador:
    timer_enable_counter(TIM4);

    uint16_t old_pos = 0, new_pos;
    while (true)
    {
        new_pos = timer_get_counter(TIM4);
        if (old_pos != new_pos) // Ante un cambio en la posición:
        {
            gpio_clear(GPIOA, GPIO0 | GPIO1 | GPIO2 | GPIO3);
            gpio_set(GPIOA, new_pos / 5); // 80 -> 16 (4 bits)
            old_pos = new_pos;
        }
    }
}
